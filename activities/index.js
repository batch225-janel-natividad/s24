const getCube = (num) => {
    return Math.pow(num, 3);
};
const num = 2;
console.log(`The cube of ${num} is 8 `);


const address = ['258','Washington Ave NW,','California', '9011'];
const [addNum, addStreet, state, zipCode] = address;


console.log(`I live at ${addNum} ${address} ${state} ${zipCode}`);



const animal = {
	name: "lolong",
	animalCategory: "crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in",

};

const { name, animalCategory, weight, measurement  } = animal;


function getAnimal ({ name, animalCategory, weight, measurement}) {
    console.log(`${name} was a saltwater ${animalCategory}. He weighted at ${weight} with a measurement of ${measurement}.`);
}

getAnimal(animal);


const arrayNum = [1,2,3,4,5];

arrayNum.forEach((num) => {
    console.log(`${num}`);
});

const add = (a,b,c,d,e) => a + b + c + d + e;
	let total = add(1,2,3,4,5);
	console.log(total);


	class dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();

myDog.name = "Frankie";
myDog.age = 5;
myDog.breed = "Bulldog";

console.log(myDog);